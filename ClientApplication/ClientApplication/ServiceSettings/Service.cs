﻿using ClientApplication.Models;
using ClientApplication.SelectionModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Timers;

namespace ClientApplication.ServiceSettings
{
    public class Service
    {
        public Timer timer { get; private set; }
        private readonly string uri = "Selections/";

        public Service()
        {
            timer = new Timer();
        }

        public async Task<List<Project>> CreateHierarhy()
        {
            try
            {
                return await Request.GetDataList<Project>(uri + "CreateHierarhy");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }

        public async Task<List<JobsCount>> JobsCount(int user_Id)
        {
            try
            {
                return await Request.GetDataList<JobsCount>(uri + $"JobsCount/{user_Id}");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }

        public async Task<List<Job>> JobsWithNameLess45Symbols(int user_Id)
        {
            try
            {
                return await Request.GetDataList<Job>(uri + $"JobsWithNameLess45Symbols/{user_Id}");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                return null;
            }
        }

        public async Task<List<JobsFinished2019>> JobsFinished2019(int user_Id)
        {
            try
            {
                return await Request.GetDataList<JobsFinished2019>(uri + $"JobsFinished2019/{user_Id}");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                return null;
            }
        }

        public async Task<List<Teams12yearsGroup>> Teams12yearsGroup()
        {
            try
            {
                return await Request.GetDataList<Teams12yearsGroup>(uri + "Teams12yearsGroup");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                return null;
            }
        }

        public async Task<List<UsersACSJobsDSC>> UsersACSJobsDSC()
        {
            try
            {
                return await Request.GetDataList<UsersACSJobsDSC>(uri + "UsersACSJobsDSC");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                return null;
            }
        }

        public async Task<List<UserStruct>> UserStruct(int user_Id)
        {
            try
            {
                return await Request.GetDataList<UserStruct>(uri + $"UserStruct/{user_Id}");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                return null;
            }
        }

        public async Task<List<ProjectStruct>> ProjectStruct(int proj_Id)
        {
            try
            {
                return await Request.GetDataList<ProjectStruct>(uri + $"ProjectStruct/{proj_Id}");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                return null;
            }
        }

        public async Task<List<Message>> GetMessages()
        {
            try
            {
                return await Request.GetDataList<Message>(uri + "GetMessages");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                return null;
            }
        }

        public async Task<int> MarkRandomTaskWithDelay(object delay)
        {
            var tcs = new TaskCompletionSource<int>();

            var listJobs = await Request.GetDataList<Job>("Jobs");

            timer = new Timer((double)delay);

            timer.Elapsed += async (sender, e) =>
            {
                try
                {
                    var result = await OnTimedEvent(listJobs);
                    Console.WriteLine($"Task with id={result} marked as completed.");
                    tcs.SetResult(result);
                }
                catch (Exception exc)
                {
                    tcs.SetException(exc);
                }
            };
            timer.AutoReset = false;
            timer.Enabled = true;

            return await tcs.Task;
        }

        private async Task<int> OnTimedEvent(List<Job> listJobs)
        {
            try
            {
                var random = new Random();
                int index = random.Next(listJobs.Count);
                var newJob = listJobs[index];
                newJob.State = State.Finished;

                return await Request.PutData<Job>("Jobs/" + newJob.Id, newJob);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                return -1;
            }
        }
    }
}
