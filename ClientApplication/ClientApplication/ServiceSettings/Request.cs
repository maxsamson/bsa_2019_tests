﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace ClientApplication.ServiceSettings
{
    public static class Request
    {
        private static HttpClient client = new HttpClient(new HttpClientHandler
        {
            ServerCertificateCustomValidationCallback =
                (sender, cert, chain, sslPolicyErrors) =>
                {
                    return true;
                }
        });
        private static JsonSerializerSettings settings = new JsonSerializerSettings();
        //private static readonly Uri _url = new Uri("http://localhost:65430/api/");

        static Request()
        {
            settings.DateFormatString = "YYYY-MM-DDTHH:mm:ss.FFFZ";
            client.BaseAddress = new Uri("http://localhost:65430/api/");
        }

        public static async Task<List<T>> GetDataList<T>(string uri)
        {
            var result = await client.GetStringAsync(uri);

            return JsonConvert.DeserializeObject<List<T>>(result, settings);
        }

        public static async Task<int> PutData<T>(string uri, T entity)
        {
            var response = client.PutAsJsonAsync(uri, entity).Result;
            if (response.IsSuccessStatusCode)
            {
                return Convert.ToInt32(await response.Content.ReadAsStringAsync());
            }
            else
                return -1;
        }
    }
}
