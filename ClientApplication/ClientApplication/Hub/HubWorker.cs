﻿using Microsoft.AspNetCore.SignalR.Client;
using System;

namespace ClientApplication.Hub
{
    public static class HubWorker
    {
        private static HubConnection connection;

        public static void ConnectionToHub()
        {
            connection = new HubConnectionBuilder()
                .WithUrl("http://localhost:65430/hub")
                .Build();
            connection.StartAsync().Wait();
            connection.On("PostResult", new Action<string>(PostResult));
        }

        public static void PostResult(string value)
        {
            Console.WriteLine($"\nMessage from Worker: {value}\n");
        }
    }
}
