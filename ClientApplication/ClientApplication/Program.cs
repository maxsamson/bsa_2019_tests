﻿using ClientApplication.Hub;
using ClientApplication.MenuPanel;
using ClientApplication.ServiceSettings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ClientApplication
{
    class Program
    {
        static public string Comand;
        //public static List<Menu> ListOfMenus = new List<Menu>();
        public static Service service;

        static async Task Main(string[] args)
        {
            HubWorker.ConnectionToHub();

            Console.Clear();
            Console.WriteLine("Write command number:");
            Console.WriteLine("[1] Start");
            Console.WriteLine("[2] Exit");
            Comand = Console.ReadLine();
            if (Comand == "1")
            {
                StartService();

                while (true)
                {
                    Console.Clear();

                    Console.WriteLine("Write command number:");
                    Console.WriteLine("[1] CreateHierarhy");
                    Console.WriteLine("[2] JobsCount");
                    Console.WriteLine("[3] JobsWithNameLess45Symbols");
                    Console.WriteLine("[4] JobsFinished2019");
                    Console.WriteLine("[5] Teams12yearsGroup");
                    Console.WriteLine("[6] UsersACSJobsDSC");
                    Console.WriteLine("[7] UserStruct");
                    Console.WriteLine("[8] ProjectStruct");
                    Console.WriteLine("[9] GetMessages");
                    Console.WriteLine("[10] Exit");
                    Console.WriteLine("[11] MarkRandomTaskWithDelay");

                    //service.MarkRandomTaskWithDelay(1000d);

                    Console.WriteLine();

                    Comand = Console.ReadLine();
                    switch (Comand)
                    {
                        case "1":
                            Console.WriteLine("CreateHierarhy triggered.");
                            await CreateHierarhy();
                            Console.WriteLine("\nPress \'Enter\' to continue.");
                            Console.ReadLine();
                            break;
                        case "2":
                            Console.WriteLine("JobsCount triggered.");
                            await JobsCount();
                            Console.WriteLine("\nPress \'Enter\' to continue.");
                            Console.ReadLine();
                            break;
                        case "3":
                            Console.WriteLine("JobsWithNameLess45Symbols triggered.");
                            await JobsWithNameLess45Symbols();
                            Console.WriteLine("\nPress \'Enter\' to continue.");
                            Console.ReadLine();
                            break;
                        case "4":
                            Console.WriteLine("JobsFinished2019 triggered.");
                            await JobsFinished2019();
                            Console.WriteLine("\nPress \'Enter\' to continue.");
                            Console.ReadLine();
                            break;
                        case "5":
                            Console.WriteLine("Teams12yearsGroup triggered.");
                            await Teams12yearsGroup();
                            Console.WriteLine("\nPress \'Enter\' to continue.");
                            Console.ReadLine();
                            break;
                        case "6":
                            Console.WriteLine("UsersACSJobsDSC triggered.");
                            await UsersACSJobsDSC();
                            Console.WriteLine("\nPress \'Enter\' to continue.");
                            Console.ReadLine();
                            break;
                        case "7":
                            Console.WriteLine("UserStruct triggered.");
                            await UserStruct();
                            Console.WriteLine("\nPress \'Enter\' to continue.");
                            Console.ReadLine();
                            break;
                        case "8":
                            Console.WriteLine("ProjectStruct triggered.");
                            await ProjectStruct();
                            Console.WriteLine("\nPress \'Enter\' to continue.");
                            Console.ReadLine();
                            break;
                        case "9":
                            Console.WriteLine("GetMessages triggered.");
                            await GetMessages();
                            Console.WriteLine("\nPress \'Enter\' to continue.");
                            Console.ReadLine();
                            break;
                        case "10":
                            service.timer.Stop();
                            service.timer.Dispose();
                            Exit();
                            break;
                        case "11":
                            Console.WriteLine("MarkRandomTaskWithDelay triggered.");
                            await service.MarkRandomTaskWithDelay(1000d);
                            Console.WriteLine("\nPress \'Enter\' to continue.");
                            Console.ReadLine();
                            break;
                        default:
                            Console.WriteLine("Enter correct number.");
                            break;
                    }
                }
            }
            else if (Comand == "2")
            {
                Exit();
            }
            else
            {
                Console.WriteLine("Incorrect comand. Press \'Enter\' to continue.");
                Console.ReadLine();
            }
        }

        private static void Exit()
        {
            Environment.Exit(0);
        }

        private static void StartService()
        {
            Console.Clear();
            service = new Service();
            //ListOfMenus[1].Show(false);
        }

        //private static async Task TurnBack()
        //{
        //    Console.Clear();
        //    //ListOfMenus[1].Show(false);
        //}

        private static async Task CreateHierarhy()
        {
            Console.Clear();
            try
            {
                var result = await service.CreateHierarhy();
                foreach (var item in result)
                {
                    Console.WriteLine($"Project id: {item.Id};");
                    Console.WriteLine($"project name: \"{item.Name}\";");
                    Console.WriteLine($"author id: {item.Author_Id};");
                    Console.WriteLine($"team id: {item.Team_Id};");
                    Console.WriteLine("Jobs:");
                    int i = 1;
                    foreach (var Job in item.Jobs)
                    {
                        Console.WriteLine($"{i++}. Job id: {Job.Id}; Job name: \"{Job.Name}\".");
                    }
                    Console.WriteLine("----------------------------------------------------------------------");
                }
            }
            catch (Exception)
            {
                Console.WriteLine("You haven't been wrote an id, try again.");
            }
            //ListOfMenus[2].Show(false);
        }

        private static async Task JobsCount()
        {
            Console.Clear();
            Console.WriteLine("Write user's id: ");
            try
            {
                int id = Convert.ToInt32(Console.ReadLine());
                var result = await service.JobsCount(id);
                if (result.Count == 0)
                    Console.WriteLine("The user with this id has no projects.");
                else
                    foreach (var item in result)
                    {
                        Console.WriteLine($"Project id: {item.Project.Id};");
                        Console.WriteLine($"Project name: \"{item.Project.Name}\";");
                        Console.WriteLine($"Jobs count: {item.Count}.");
                        Console.WriteLine("------------------------------------------");
                    }
            }
            catch (Exception)
            {
                Console.WriteLine("You haven't been wrote an id, try again.");
            }
            //ListOfMenus[2].Show(false);
        }

        private static async Task JobsWithNameLess45Symbols()
        {
            Console.Clear();
            Console.WriteLine("Write user's id: ");
            try
            {
                int id = Convert.ToInt32(Console.ReadLine());
                var result = await service.JobsWithNameLess45Symbols(id);
                if (result.Count == 0)
                    Console.WriteLine("There are no Jobs that satisfy this condition.");
                else
                    foreach (var item in result)
                    {
                        Console.WriteLine($"Job id: {item.Id};");
                        Console.WriteLine($"Job name: \"{item.Name}\".");
                        Console.WriteLine("-------------------------------------");
                    }
            }
            catch
            {
                Console.WriteLine("You haven't been wrote an id, try again.");
            }
            //ListOfMenus[2].Show(false);
        }

        private static async Task JobsFinished2019()
        {
            Console.Clear();
            Console.WriteLine("Write user's id: ");
            try
            {
                int id = Convert.ToInt32(Console.ReadLine());
                var result = await service.JobsFinished2019(id);
                if (result.Count == 0)
                    Console.WriteLine("The user has no such Jobs.");
                else
                    foreach (var item in result)
                    {
                        Console.WriteLine($"Job id: {item.Id};");
                        Console.WriteLine($"Job name: \"{item.Name}\".");
                        Console.WriteLine("---------------------------------------");
                    }
            }
            catch
            {
                Console.WriteLine("You haven't been wrote an id, try again.");
            }
            //ListOfMenus[2].Show(false);
        }

        private static async Task Teams12yearsGroup()
        {
            try
            {
                Console.Clear();
                var result = await service.Teams12yearsGroup();
                if (result.Count == 0)
                    Console.WriteLine("There are no teams that satisfy this condition.");
                else
                    foreach (var item in result)
                    {
                        Console.WriteLine($"Team id: {item.Id};");
                        Console.WriteLine($"Team name: \"{item.Name}\";");
                        Console.WriteLine("Users:");
                        int i = 1;
                        foreach (var user in item.Users)
                        {
                            Console.WriteLine($"{i++}. User id: {user.Id}; User name: \"{user.First_Name} {user.Last_Name}\".");
                        }
                        Console.WriteLine("----------------------------------------");
                    }
            }
            catch
            {
                Console.WriteLine("You haven't been wrote an id, try again.");
            }
            //ListOfMenus[2].Show(false);
        }

        private static async Task UsersACSJobsDSC()
        {
            try
            {
                Console.Clear();
                var result = await service.UsersACSJobsDSC();
                foreach (var item in result)
                {
                    Console.WriteLine($"User id: {item.User.Id};");
                    Console.WriteLine($"User name: \"{item.User.First_Name} {item.User.Last_Name}\";");
                    Console.WriteLine("Jobs:");
                    int i = 1;
                    foreach (var Job in item.Jobs)
                    {
                        Console.WriteLine($"{i++}. Job id: {Job.Id}; Job name: \"{Job.Name}\".");
                    }
                    Console.WriteLine("--------------------------------------------");
                }
            }
            catch
            {
                Console.WriteLine("You haven't been wrote an id, try again.");
            }
            //ListOfMenus[2].Show(false);
        }

        private static async Task UserStruct()
        {
            Console.Clear();
            Console.WriteLine("Write user's id: ");
            try
            {
                int id = Convert.ToInt32(Console.ReadLine());
                var result = (await service.UserStruct(id)).First();

                Console.WriteLine($"User id: {result.User.Id};");
                Console.WriteLine($"User name: \"{result.User.First_Name} {result.User.Last_Name}\";");
                Console.WriteLine($"Last project id: {result.LastProject.Id};");
                Console.WriteLine($"Last project name: \"{result.LastProject.Name}\";");
                Console.WriteLine($"Last project's Jobs count: {result.LastProjectJobsCount}");
                Console.WriteLine($"Not finished or canceled Jobs:");
                int i = 1;
                foreach (var Job in result.NotFinishedCanceledJobs)
                {
                    Console.WriteLine($"{i++}. Job id: {Job.Id}; Job name: \"{Job.Name}\".");
                }
                Console.WriteLine($"Longest Job id: {result.MaxDurationJob.Id};");
                Console.WriteLine($"Longest Job name: \"{result.MaxDurationJob.Name}\".");
                Console.WriteLine("--------------------------------------");
            }
            catch
            {
                Console.WriteLine("You haven't been wrote an id, try again.");
            }
            //ListOfMenus[2].Show(false);
        }

        private static async Task ProjectStruct()
        {
            Console.Clear();
            Console.WriteLine("Write project's id: ");
            try
            {
                int id = Convert.ToInt32(Console.ReadLine());
                var result = (await service.ProjectStruct(id)).FirstOrDefault();

                Console.WriteLine($"Project id: {result.Project.Id};");
                Console.WriteLine($"Project name: \"{result.Project.Name}\";");
                if (result.LongestJob != null)
                {
                    Console.WriteLine($"Longest Job id: {result.LongestJob.Id};");
                    Console.WriteLine($"Longest Job name: \"{result.LongestJob.Name}\";");
                }
                else
                    Console.WriteLine("Project has not Jobs.");
                if (result.LongestJob != null)
                {
                    Console.WriteLine($"Shortest Job id: {result.ShortestJob.Id};");
                    Console.WriteLine($"Shortest Job name: \"{result.ShortestJob.Name}\";");
                }
                else
                    Console.WriteLine("Project has not Jobs.");
                Console.WriteLine($"Users count: \"{result.UsersCount}\".");
                Console.WriteLine("--------------------------------------");
            }
            catch
            {
                Console.WriteLine("You haven't been wrote an id, try again.");
            }
            //ListOfMenus[2].Show(false);
        }

        private static async Task GetMessages()
        {
            Console.Clear();
            var result = await service.GetMessages();
            if (result == null)
                Console.WriteLine("Something wrong with file reading.");
            else if (result.Count == 0)
                Console.WriteLine("There are no messages in log file.");
            else
            {
                foreach (var item in result)
                {
                    Console.WriteLine($"{item.Create_At}: {item.Text}");
                }
            }

            //ListOfMenus[2].Show(false);
        }
    }
}
