﻿using ClientApplication.Models;
using System.Collections.Generic;

namespace ClientApplication.SelectionModels
{
    public class UsersACSJobsDSC
    {
        public User User { get; set; }
        public List<Job> Jobs { get; set; }
    }
}
