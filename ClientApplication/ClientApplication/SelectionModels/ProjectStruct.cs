﻿using ClientApplication.Models;

namespace ClientApplication.SelectionModels
{
    public class ProjectStruct
    {
        public Project Project { get; set; }
        public Job LongestJob { get; set; }
        public Job ShortestJob { get; set; }
        public int UsersCount { get; set; }
    }
}
