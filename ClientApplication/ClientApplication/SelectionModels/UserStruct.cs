﻿using ClientApplication.Models;
using System.Collections.Generic;

namespace ClientApplication.SelectionModels
{
    public class UserStruct
    {
        public User User { get; set; }
        public Project LastProject { get; set; }
        public int LastProjectJobsCount { get; set; }
        public List<Job> NotFinishedCanceledJobs { get; set; }
        public Job MaxDurationJob { get; set; }
    }
}
