﻿using System;
using System.Threading.Tasks;
using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Nito.AsyncEx;
using Project_Structure.BLL.Interfaces;
using Project_Structure.Shared.DTO;
using Project_Structure.Shared.Exceptions;

namespace Project_Structure.Controllers
{
    [Route("api/Jobs")]
    [ApiController]
    public class JobsController : ControllerBase
    {
        private readonly AsyncLock _asyncLock = new AsyncLock();
        readonly IJobService service;

        public JobsController(IJobService JobService)
        {
            service = JobService;
        }

        // GET: api/Jobs
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                using (await _asyncLock.LockAsync())
                {
                    return Ok(await service.GetAll());
                }
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        // GET: api/Jobs/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                using (await _asyncLock.LockAsync())
                {
                    return Ok(await service.GetById(id));
                }
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        // POST: v1/api/Jobs
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]JobDTO Job)
        {
            try
            {
                using (await _asyncLock.LockAsync())
                {
                    return Ok(await service.Create(Job));
                }
            }
            catch (ValidationException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        // PUT: v1/api/Jobs/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] JobDTO Job)
        {
            try
            {
                using (await _asyncLock.LockAsync())
                {
                    service.Update(id, Job);
                    return Ok(id);
                }
            }
            catch (NotFoundException)
            {
                return NotFound();
            }
            catch (ValidationException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        // DELETE: v1/api/Jobs/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                using (await _asyncLock.LockAsync())
                {
                    return Ok(service.DeleteById(id));
                }
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        // DELETE: v1/api/Jobs
        [HttpDelete]
        public async Task<IActionResult> Delete([FromBody] JobDTO Job)
        {
            try
            {
                using (await _asyncLock.LockAsync())
                {
                    return Ok(service.Delete(Job));
                }
            }
            catch (Exception)
            {
                return NotFound();
            }
        }
    }
}