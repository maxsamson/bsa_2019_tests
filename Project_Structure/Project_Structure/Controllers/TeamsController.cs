﻿using System;
using System.Threading.Tasks;
using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Nito.AsyncEx;
using Project_Structure.BLL.Interfaces;
using Project_Structure.Shared.DTO;
using Project_Structure.Shared.Exceptions;

namespace Project_Structure.Controllers
{
    [Route("api/Teams")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly AsyncLock _asyncLock = new AsyncLock();
        readonly ITeamService service;

        public TeamsController(ITeamService teamService)
        {
            service = teamService;
        }

        // GET: api/Teams
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                using (await _asyncLock.LockAsync())
                {
                    return Ok(await service.GetAll());
                }
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        // GET: api/Teams/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                using (await _asyncLock.LockAsync())
                {
                    return Ok(await service.GetById(id));
                }
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        // POST: v1/api/Teams
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]TeamDTO team)
        {
            try
            {
                using (await _asyncLock.LockAsync())
                {
                    return Ok(await service.Create(team));
                }
            }
            catch (ValidationException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        // PUT: v1/api/Teams/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] TeamDTO team)
        {
            try
            {
                using (await _asyncLock.LockAsync())
                {
                    return Ok(service.Update(id, team));
                }
            }
            catch (NotFoundException)
            {
                return NotFound();
            }
            catch (ValidationException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        // DELETE: v1/api/Teams/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                using (await _asyncLock.LockAsync())
                {
                    return Ok(service.DeleteById(id));
                }
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        // DELETE: v1/api/Teams
        [HttpDelete]
        public async Task<IActionResult> Delete([FromBody] TeamDTO team)
        {
            try
            {
                using (await _asyncLock.LockAsync())
                {
                    return Ok(service.Delete(team));
                }
            }
            catch (Exception)
            {
                return NotFound();
            }
        }
    }
}