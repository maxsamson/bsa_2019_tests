﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Nito.AsyncEx;
using Project_Structure.BLL.Interfaces;

namespace Project_Structure.Controllers
{
    [Route("api/Selections")]
    [ApiController]
    public class SelectionController : ControllerBase
    {
        private readonly AsyncLock _asyncLock = new AsyncLock();

        private readonly IConfiguration configuration;
        readonly ISelectionService service;

        public SelectionController(ISelectionService selectionService, IConfiguration configuration)
        {
            service = selectionService;
            this.configuration = configuration;
        }

        // GET: api/Selections/CreateHierarhy
        [Route("CreateHierarhy")]
        [HttpGet]
        public async Task<IActionResult> CreateHierarhy()
        {
            try
            {
                using (await _asyncLock.LockAsync())
                {
                    return Ok(await service.CreateHierarhy());
                }
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        // GET: api/Selections/JobsCount
        [Route("JobsCount/{id:int}")]
        [HttpGet]
        public async Task<IActionResult> JobsCount(int id)
        {
            try
            {
                using (await _asyncLock.LockAsync())
                {
                    return Ok(await service.JobsCount(id));
                }
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        // GET: api/Selections/JobsWithNameLess45Symbols
        [Route("JobsWithNameLess45Symbols/{id:int}")]
        [HttpGet]
        public async Task<IActionResult> JobsWithNameLess45Symbols(int id)
        {
            try
            {
                using (await _asyncLock.LockAsync())
                {
                    return Ok(await service.JobsWithNameLess45Symbols(id));
                }
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        // GET: api/Selections/JobsFinished2019
        [Route("JobsFinished2019/{id:int}")]
        [HttpGet]
        public async Task<IActionResult> JobsFinished2019(int id)
        {
            try
            {
                using (await _asyncLock.LockAsync())
                {
                    return Ok(await service.JobsFinished2019(id));
                }
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        // GET: api/Selections/Teams12yearsGroup
        [Route("Teams12yearsGroup")]
        [HttpGet]
        public async Task<IActionResult> Teams12yearsGroup()
        {
            try
            {
                using (await _asyncLock.LockAsync())
                {
                    return Ok(await service.Teams12yearsGroup());
                }
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        // GET: api/Selections/UsersACSJobsDSC
        [Route("UsersACSJobsDSC")]
        [HttpGet]
        public async Task<IActionResult> UsersACSJobsDSC()
        {
            try
            {
                using (await _asyncLock.LockAsync())
                {
                    return Ok(await service.UsersACSJobsDSC());
                }
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        // GET: api/Selections/UserStruct
        [Route("UserStruct/{id:int}")]
        [HttpGet]
        public async Task<IActionResult> UserStruct(int id)
        {
            try
            {
                using (await _asyncLock.LockAsync())
                {
                    return Ok(await service.UserStruct(id));
                }
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        // GET: api/Selections/ProjectStruct
        [Route("ProjectStruct/{id:int}")]
        [HttpGet]
        public async Task<IActionResult> ProjectStruct(int id)
        {
            try
            {
                using (await _asyncLock.LockAsync())
                {
                    return Ok(await service.ProjectStruct(id));
                }
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        // GET: api/Selections/GetMessages
        [Route("GetMessages")]
        [HttpGet]
        public async Task<IActionResult> GetMessages()
        {
            try
            {
                using (await _asyncLock.LockAsync())
                {
                    return Ok(await service.GetMessages(configuration["LogFile"]));
                }
            }
            catch (Exception)
            {
                return NotFound();
            }
        }
    }
}