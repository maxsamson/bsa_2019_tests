﻿using System;
using System.Threading.Tasks;
using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Nito.AsyncEx;
using Project_Structure.BLL.Interfaces;
using Project_Structure.Shared.DTO;
using Project_Structure.Shared.Exceptions;

namespace Project_Structure.Controllers
{
    [Route("api/Users")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly AsyncLock _asyncLock = new AsyncLock();
        readonly IUserService service;

        public UsersController(IUserService userService)
        {
            service = userService;
        }

        // GET: api/Users
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                using (await _asyncLock.LockAsync())
                {
                    return Ok(await service.GetAll());
                }
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        // GET: api/Users/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                using (await _asyncLock.LockAsync())
                {
                    return Ok(await service.GetById(id));
                }
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        // POST: v1/api/Users
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]UserDTO user)
        {
            try
            {
                using (await _asyncLock.LockAsync())
                {
                    return Ok(await service.Create(user));
                }
            }
            catch (ValidationException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        // PUT: v1/api/Users/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] UserDTO user)
        {
            try
            {
                using (await _asyncLock.LockAsync())
                {
                    return Ok(service.Update(id, user));
                }
            }
            catch (NotFoundException)
            {
                return NotFound();
            }
            catch (ValidationException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        // DELETE: v1/api/Users/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                using (await _asyncLock.LockAsync())
                {
                    return Ok(service.DeleteById(id));
                }
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        // DELETE: v1/api/Users
        [HttpDelete]
        public async Task<IActionResult> Delete([FromBody] UserDTO user)
        {
            try
            {
                using (await _asyncLock.LockAsync())
                {
                    return Ok(service.Delete(user));
                }
            }
            catch (Exception)
            {
                return NotFound();
            }
        }
    }
}