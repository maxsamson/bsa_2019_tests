﻿using DAL.Interfaces;
using DAL.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructureTests.Fake
{
    public class FakeRepository<T> : IRepository<T> where T : Entity
    {
        public List<T> Collection { get; set; }

        public FakeRepository()
        {
            Collection = new List<T>();
        }

        public async Task<int> Create(T entity)
        {
            entity.Id = Collection.Count + 1;
            Collection.Add(entity);
            return entity.Id;
        }

        public async Task Delete(T entity)
        {
            Collection.Remove(entity);
        }

        public async Task DeleteById(int id)
        {
            Collection.RemoveAt(id - 1);
        }

        public async Task<List<T>> GetAll()
        {
            return Collection;
        }

        public async Task<T> GetById(int id)
        {
            return Collection.SingleOrDefault(entity => entity.Id == id);
        }

        public async Task Update(T entity)
        {
            Collection[entity.Id - 1] = entity;
        }
    }
}
