﻿using DAL.Models;
using FluentValidation;
using Microsoft.Extensions.Configuration;
using Moq;
using NUnit.Framework;
using Project_Structure.BLL.Hubs;
using Project_Structure.BLL.Interfaces;
using Project_Structure.BLL.Mapping;
using Project_Structure.BLL.Services;
using Project_Structure.BLL.Validators;
using Project_Structure.Shared.DTO;
using ProjectStructureTests.Fake;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructureTests.ServiceTests
{
    public class TeamServiceTests
    {
        private readonly FakeRepository<Team> teamRepo;
        private readonly FakeRepository<Project> projectRepo;
        private readonly FakeRepository<Job> jobRepo;
        private readonly FakeRepository<User> userRepo;

        private readonly IMapper mapper;
        private readonly AbstractValidator<TeamDTO> validator;
        private readonly TeamService service;

        public TeamServiceTests()
        {
            teamRepo = new FakeRepository<Team>();
            projectRepo = new FakeRepository<Project>();
            jobRepo = new FakeRepository<Job>();
            userRepo = new FakeRepository<User>();

            mapper = new Mapper(projectRepo, jobRepo, teamRepo, userRepo);
            validator = new TeamValidator();
            service = new TeamService(teamRepo, mapper, validator,
                new Mock<QueueService>(new Mock<IConfiguration>().Object, new Mock<ProjectHub>().Object).Object);
        }

        [SetUp]
        public void TestSetup()
        {
            var jobList = new List<Job>()
            {
                new Job
                    {
                        Id = 1,
                        Name = "Eaque corporis illum ut.",
                        Description = "Inventore quae alias magnam sed qui sint velit est.\nSuscipit dolor ut omnis voluptate.\nExercitationem qui et temporibus dignissimos autem.\nSapiente recusandae illo maiores quasi.\nEt commodi harum voluptatem cum.",
                        Created_At = new DateTime(2019, 6, 16, 2, 54, 26),
                        Finished_At = new DateTime(2020, 2, 1, 17, 12, 48),
                        State = State.Started
                    }
            };

            var userList = new List<User>()
            {
                new User
                {
                    Id = 1,
                    First_Name = "Gayle1",
                    Last_Name = "Swift",
                    Email = "Gayle_Swift98@yahoo.com",
                    Birthday = new DateTime(2013, 11, 7, 13, 1, 47),
                    Registered_At = new DateTime(2019, 6, 17, 13, 1, 47),
                    Projects = new List<Project>(),
                    Jobs = new List<Job>(),
                }
            };
            jobList[0].User = userList[0];
            userList[0].Jobs = new List<Job>() { jobList[0] };

            var teamList = new List<Team>()
            {
                new Team
                {
                    Id = 1,
                    Name = "labore",
                    Created_At = new DateTime(2019, 6, 19, 1, 0, 24),
                    Users = new List<User>(),
                    Projects = new List<Project>()
                }
            };
            userList[0].Team = teamList[0];
            teamList[0].Users = new List<User>() { userList[0] };

            var projectList = new List<Project>()
            {
                new Project
                {
                    Id = 1,
                    Name = "Rerum voluptatem beatae nesciunt consectetur.",
                    Description = "Ut quisquam molestias ut.\nQuia laudantium est quo asperiores veritatis porro ut quod doloribus.\nReiciendis odit ut hic libero.\nFuga debitis veniam ut.\nEos modi dolorem quia accusamus asperiores a et molestiae.\nAut eius et nihil voluptatem.",
                    Created_At = new DateTime(2019, 6, 18, 13, 46, 19),
                    Deadline = new DateTime(2020, 1, 13, 6, 24, 33),
                    Jobs = new List<Job>()
                }
            };
            jobList[0].Project = projectList[0];
            userList[0].Projects.Add(projectList[0]);
            teamList[0].Projects.Add(projectList[0]);
            projectList[0].Author = userList[0];
            projectList[0].Team = teamList[0];
            projectList[0].Jobs = new List<Job>() { jobList[0] };

            jobRepo.Collection = jobList;
            userRepo.Collection = userList;
            teamRepo.Collection = teamList;
            projectRepo.Collection = projectList;
        }

        [Test]
        public async Task UpdateStateTiFinished_When_JobNotFinished_Then_JobFinished()
        {
            var team = new TeamDTO()
            {
                Id = 1,
                Name = "labore",
                Created_At = new DateTime(2019, 6, 19, 1, 0, 24),
                Users = new List<UserDTO>()
                {
                    new UserDTO()
                    {
                        Id = 1,
                        First_Name = "Gayle1",
                        Last_Name = "Swift",
                        Email = "Gayle_Swift98@yahoo.com",
                        Birthday = new DateTime(2013, 11, 7, 13, 1, 47),
                        Registered_At = new DateTime(2019, 6, 17, 13, 1, 47),
                        Projects = new List<ProjectDTO>()
                        {
                            new ProjectDTO()
                            {
                                Id = 1,
                                Name = "Rerum voluptatem beatae nesciunt consectetur.",
                                Description = "Ut quisquam molestias ut.\nQuia laudantium est quo asperiores veritatis porro ut quod doloribus.\nReiciendis odit ut hic libero.\nFuga debitis veniam ut.\nEos modi dolorem quia accusamus asperiores a et molestiae.\nAut eius et nihil voluptatem.",
                                Created_At = new DateTime(2019, 6, 18, 13, 46, 19),
                                Deadline = new DateTime(2020, 1, 13, 6, 24, 33),
                                Jobs = new List<JobDTO>()
                                {
                                    new JobDTO()
                                    {
                                        Id = 1,
                                        Name = "Eaque corporis illum ut.",
                                        Description = "Inventore quae alias magnam sed qui sint velit est.\nSuscipit dolor ut omnis voluptate.\nExercitationem qui et temporibus dignissimos autem.\nSapiente recusandae illo maiores quasi.\nEt commodi harum voluptatem cum.",
                                        Created_At = new DateTime(2019, 6, 16, 2, 54, 26),
                                        Finished_At = new DateTime(2020, 2, 1, 17, 12, 48),
                                        State = State.Finished,
                                        Performer_Id = 1,
                                        Project_Id = 1
                                    }
                                },
                                Author_Id = 1,
                                Team_Id = 1
                            }
                        },
                        Jobs = new List<JobDTO>()
                        {
                            new JobDTO()
                                    {
                                        Id = 1,
                                        Name = "Eaque corporis illum ut.",
                                        Description = "Inventore quae alias magnam sed qui sint velit est.\nSuscipit dolor ut omnis voluptate.\nExercitationem qui et temporibus dignissimos autem.\nSapiente recusandae illo maiores quasi.\nEt commodi harum voluptatem cum.",
                                        Created_At = new DateTime(2019, 6, 16, 2, 54, 26),
                                        Finished_At = new DateTime(2020, 2, 1, 17, 12, 48),
                                        State = State.Finished,
                                        Performer_Id = 1,
                                        Project_Id = 1
                                    }
                        },
                        Team_Id = 1
                    }
                },
                Projects = new List<ProjectDTO>()
                {
                    new ProjectDTO()
                    {
                        Id = 1,
                        Name = "Rerum voluptatem beatae nesciunt consectetur.",
                        Description = "Ut quisquam molestias ut.\nQuia laudantium est quo asperiores veritatis porro ut quod doloribus.\nReiciendis odit ut hic libero.\nFuga debitis veniam ut.\nEos modi dolorem quia accusamus asperiores a et molestiae.\nAut eius et nihil voluptatem.",
                        Created_At = new DateTime(2019, 6, 18, 13, 46, 19),
                        Deadline = new DateTime(2020, 1, 13, 6, 24, 33),
                        Jobs = new List<JobDTO>()
                        {
                            new JobDTO()
                            {
                                Id = 1,
                                Name = "Eaque corporis illum ut.",
                                Description = "Inventore quae alias magnam sed qui sint velit est.\nSuscipit dolor ut omnis voluptate.\nExercitationem qui et temporibus dignissimos autem.\nSapiente recusandae illo maiores quasi.\nEt commodi harum voluptatem cum.",
                                Created_At = new DateTime(2019, 6, 16, 2, 54, 26),
                                Finished_At = new DateTime(2020, 2, 1, 17, 12, 48),
                                State = State.Finished,
                                Performer_Id = 1,
                                Project_Id = 1
                            }
                        },
                        Author_Id = 1,
                        Team_Id = 1
                    }
                }
            };

            var user = new UserDTO()
            {
                Id = 2,
                First_Name = "Andrii",
                Last_Name = "Sytnyk",
                Email = "Gayle_Swift98@yahoo.com",
                Birthday = new DateTime(2013, 11, 7, 13, 1, 47),
                Registered_At = new DateTime(2019, 6, 17, 13, 1, 47),
                Projects = new List<ProjectDTO>()
                {
                    new ProjectDTO()
                    {
                        Id = 1,
                        Name = "Rerum voluptatem beatae nesciunt consectetur.",
                        Description = "Ut quisquam molestias ut.\nQuia laudantium est quo asperiores veritatis porro ut quod doloribus.\nReiciendis odit ut hic libero.\nFuga debitis veniam ut.\nEos modi dolorem quia accusamus asperiores a et molestiae.\nAut eius et nihil voluptatem.",
                        Created_At = new DateTime(2019, 6, 18, 13, 46, 19),
                        Deadline = new DateTime(2020, 1, 13, 6, 24, 33),
                        Jobs = new List<JobDTO>()
                        {
                            new JobDTO()
                            {
                                Id = 1,
                                Name = "Eaque corporis illum ut.",
                                Description = "Inventore quae alias magnam sed qui sint velit est.\nSuscipit dolor ut omnis voluptate.\nExercitationem qui et temporibus dignissimos autem.\nSapiente recusandae illo maiores quasi.\nEt commodi harum voluptatem cum.",
                                Created_At = new DateTime(2019, 6, 16, 2, 54, 26),
                                Finished_At = new DateTime(2020, 2, 1, 17, 12, 48),
                                State = State.Finished,
                                Performer_Id = 1,
                                Project_Id = 1
                            }
                        },
                        Author_Id = 1,
                        Team_Id = 1
                    }
                },
                Jobs = new List<JobDTO>()
                {
                    new JobDTO()
                    {
                        Id = 1,
                        Name = "Eaque corporis illum ut.",
                        Description = "Inventore quae alias magnam sed qui sint velit est.\nSuscipit dolor ut omnis voluptate.\nExercitationem qui et temporibus dignissimos autem.\nSapiente recusandae illo maiores quasi.\nEt commodi harum voluptatem cum.",
                        Created_At = new DateTime(2019, 6, 16, 2, 54, 26),
                        Finished_At = new DateTime(2020, 2, 1, 17, 12, 48),
                        State = State.Finished,
                        Performer_Id = 1,
                        Project_Id = 1
                    }
                },
                Team_Id = 1
            };
            userRepo.Collection.Add(await mapper.MapUser(user));

            await service.UpdateTeamAddUser(1, user);

            Assert.AreEqual(teamRepo.Collection[0].Users.Count, 2);
            Assert.AreEqual(teamRepo.Collection[0].Users[1].Id, user.Id);
            Assert.AreEqual(teamRepo.Collection[0].Users[1].First_Name, user.First_Name);
            Assert.AreEqual(teamRepo.Collection[0].Users[1].Last_Name, user.Last_Name);
        }
    }
}
