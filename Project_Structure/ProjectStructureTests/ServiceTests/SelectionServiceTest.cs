using DAL.Models;
using Microsoft.Extensions.Configuration;
using Moq;
using NUnit.Framework;
using Project_Structure.BLL.Hubs;
using Project_Structure.BLL.Interfaces;
using Project_Structure.BLL.Mapping;
using Project_Structure.BLL.Services;
using Project_Structure.Shared.DTO;
using Project_Structure.Shared.SelectionModels;
using ProjectStructureTests.Fake;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructureTests.ServiceTests
{
    public class SelectionServiceTest
    {
        private readonly FakeRepository<Project> projectRepo;
        private readonly FakeRepository<Job> jobRepo;
        private readonly FakeRepository<Team> teamRepo;
        private readonly FakeRepository<User> userRepo;

        private readonly IMapper mapper;
        private readonly SelectionService service;

        public SelectionServiceTest()
        {
            projectRepo = new FakeRepository<Project>();
            jobRepo = new FakeRepository<Job>();
            teamRepo = new FakeRepository<Team>();
            userRepo = new FakeRepository<User>();

            mapper = new Mapper(projectRepo, jobRepo, teamRepo, userRepo);
            service = new SelectionService(projectRepo, jobRepo, teamRepo, 
                userRepo, mapper, new Mock<QueueService>(new Mock<IConfiguration>().Object, new Mock<ProjectHub>().Object).Object);
        }

        [SetUp]
        public void TestSetup()
        {
            var jobList = new List<Job>()
            {
                new Job
                    {
                        Id = 1,
                        Name = "Eaque corporis illum ut.",
                        Description = "Inventore quae alias magnam sed qui sint velit est.\nSuscipit dolor ut omnis voluptate.\nExercitationem qui et temporibus dignissimos autem.\nSapiente recusandae illo maiores quasi.\nEt commodi harum voluptatem cum.",
                        Created_At = new DateTime(2019, 6, 16, 2, 54, 26),
                        Finished_At = new DateTime(2020, 2, 1, 17, 12, 48),
                        State = State.Started
                    },
                new Job
                {
                    Id = 2,
                    Name = "Debitis quis ad quas voluptatem voluptatem.",
                    Description = "Similique molestiae esse eius nihil repudiandae possimus eos ea nobis.\nEaque ipsam atque sequi dignissimos fugiat voluptate hic nihil.\nNatus officia quis.\nDeleniti sit earum eveniet.\nCumque magni quis eaque atque natus expedita ratione illum.",
                    Created_At = new DateTime(2019, 6, 18, 19, 31, 15),
                    Finished_At = new DateTime(2019, 8, 9, 3, 39, 45),
                    State = State.Finished
                },
                new Job
                {
                        Id = 3,
                        Name = "Officia quas dolorem quod veniam vitae odit voluptates.",
                    Description = "Ut et consequatur quo.\nIpsa rerum qui.\nDelectus sunt voluptatibus facilis facere minima repellat deserunt temporibus.",
                    Created_At = new DateTime(2019, 6, 19, 0, 56, 43),
                    Finished_At = new DateTime(2020, 3, 31, 17, 26, 25),
                    State = State.Started
                },
                new Job
                {
                        Id = 4,
                        Name = "Aut occaecati officiis dolor culpa porro.",
                    Description = "Voluptatem ipsum deleniti quisquam est aut accusamus porro veritatis quia.\nOccaecati et excepturi eius temporibus quia distinctio aspernatur.\nSoluta qui consequuntur est possimus itaque dolores ut.\nFacilis occaecati cupiditate omnis cumque.",
                    Created_At = new DateTime(2019, 6, 19, 1, 1, 47),
                    Finished_At = new DateTime(2019, 7, 7, 16, 54, 47),
                    State = State.Started
                },
                new Job
                {
                        Id = 5,
                        Name = "Eos adipisci dignissimos minus non est est minus.",
                    Description = "Possimus est asperiores repellendus tenetur placeat.\nDolor ipsa facere delectus tempore.\nUt eum esse et dolor delectus repudiandae vitae aut enim.\nDeleniti sint delectus saepe autem.",
                    Created_At = new DateTime(2019, 6, 18, 21, 44, 18),
                    Finished_At = new DateTime(2020, 1, 11, 8, 49, 6),
                    State = State.Finished
                }
            };

            var userList = new List<User>()
            {
                new User
                {
                    Id = 1,
                    First_Name = "Gayle1",
                    Last_Name = "Swift",
                    Email = "Gayle_Swift98@yahoo.com",
                    Birthday = new DateTime(2013, 11, 7, 13, 1, 47),
                    Registered_At = new DateTime(2019, 6, 17, 13, 1, 47),
                    Projects = new List<Project>(),
                    Jobs = new List<Job>(),
                },
                new User
                {
                    Id = 2,
                    First_Name = "Gayle2",
                    Last_Name = "Swift",
                    Email = "Gayle_Swift98@yahoo.com",
                    Birthday = new DateTime(2013, 11, 7, 13, 1, 47),
                    Registered_At = new DateTime(2019, 6, 17, 13, 1, 47),
                    Projects = new List<Project>(),
                    Jobs = new List<Job>(),
                },
                new User
                {
                    Id = 3,
                    First_Name = "Gayle3",
                    Last_Name = "Swift",
                    Email = "Gayle_Swift98@yahoo.com",
                    Birthday = new DateTime(2013, 11, 7, 13, 1, 47),
                    Registered_At = new DateTime(2019, 6, 17, 13, 1, 47),
                    Projects = new List<Project>(),
                    Jobs = new List<Job>(),
                }
            };

            jobList[0].User = userList[0];
            jobList[1].User = userList[0];
            userList[0].Jobs = new List<Job>() { jobList[0], jobList[1] };

            jobList[2].User = userList[1];
            userList[1].Jobs = new List<Job>() { jobList[2] };

            jobList[3].User = userList[2];
            jobList[4].User = userList[2];
            userList[2].Jobs = new List<Job>() { jobList[3], jobList[4] };

            var teamList = new List<Team>()
            {
                new Team
                {
                    Id = 1,
                    Name = "labore",
                    Created_At = new DateTime(2019, 6, 19, 1, 0, 24),
                    Users = new List<User>(),
                    Projects = new List<Project>()
                },
                new Team
                {
                    Id = 2,
                    Name = "sunt",
                    Created_At = new DateTime(2019, 6, 19, 6, 38, 3),
                    Users = new List<User>(),
                    Projects = new List<Project>()
                }
            };

            userList[0].Team = teamList[0];
            userList[1].Team = teamList[0];
            teamList[0].Users = new List<User>() { userList[0], userList[1] };

            userList[2].Team = teamList[1];
            teamList[1].Users = new List<User>() { userList[2] };

            var projectList = new List<Project>()
            {
                new Project
                {
                    Id = 1,
                    Name = "Rerum voluptatem beatae nesciunt consectetur.",
                    Description = "Ut quisquam molestias ut.\nQuia laudantium est quo asperiores veritatis porro ut quod doloribus.\nReiciendis odit ut hic libero.\nFuga debitis veniam ut.\nEos modi dolorem quia accusamus asperiores a et molestiae.\nAut eius et nihil voluptatem.",
                    Created_At = new DateTime(2019, 6, 18, 13, 46, 19),
                    Deadline = new DateTime(2020, 1, 13, 6, 24, 33),
                    Jobs = new List<Job>()
                },
                new Project
                {
                    Id = 2,
                    Name = "Atque quia et optio aut.",
                    Description = "Non non aperiam aspernatur et est mollitia enim quia nihil.\nLibero adipisci quisquam earum fugiat et perferendis explicabo.",
                    Created_At = new DateTime(2019, 6, 18, 20, 12, 38),
                    Deadline = new DateTime(2020, 5, 9, 12, 34, 30),
                    Jobs = new List<Job>()
                },
                new Project
                {
                    Id = 3,
                    Name = "Harum rerum ipsa quidem et.",
                    Description = "Cupiditate veritatis ad rerum veritatis molestiae.\nDolorem soluta adipisci fuga iste aut est eveniet et.\nMolestiae ut nisi officia nobis consequatur neque sint.\nAut in non officia illo minus.\nVoluptas ut est qui dolore distinctio sint delectus.",
                    Created_At = new DateTime(2019, 6, 18, 23, 17, 38),
                    Deadline = new DateTime(2019, 9, 14, 16, 45, 32),
                    Jobs = new List<Job>()
                }
            };

            jobList[0].Project = projectList[0];
            jobList[4].Project = projectList[0];
            userList[0].Projects.Add(projectList[0]);
            teamList[1].Projects.Add(projectList[0]);
            projectList[0].Author = userList[0];
            projectList[0].Team = teamList[1];
            projectList[0].Jobs = new List<Job>() { jobList[0], jobList[4] };

            jobList[1].Project = projectList[1];
            jobList[3].Project = projectList[1];
            userList[1].Projects.Add(projectList[1]);
            teamList[0].Projects.Add(projectList[1]);
            projectList[1].Author = userList[1];
            projectList[1].Team = teamList[0];
            projectList[1].Jobs = new List<Job>() { jobList[1], jobList[3] };

            jobList[2].Project = projectList[2];
            userList[1].Projects.Add(projectList[2]);
            teamList[1].Projects.Add(projectList[2]);
            projectList[2].Author = userList[1];
            projectList[2].Team = teamList[1];
            projectList[2].Jobs = new List<Job>() { jobList[2] };

            jobRepo.Collection = jobList;
            userRepo.Collection = userList;
            teamRepo.Collection = teamList;
            projectRepo.Collection = projectList;
        }

        [Test]
        public async Task JobsCount_When_UserHasProjectsAndProjectsHaveSomeJobs_Then_ReturnCountOfJobsForEachProject()
        {
            var list = await service.JobsCount(2);

            foreach (var item in list)
            {
                if (item.Project.Id == 2)
                    Assert.AreEqual(item.Count, 2);
                else
                    Assert.AreEqual(item.Count, 1);
            }
        }

        [Test]
        public async Task JobsWithNameLess45Symbols_When_UserHasTasksWithNameLess45Symbols_Then_ReturnJobsWithNameLess45Symbols()
        {
            var list = await service.JobsWithNameLess45Symbols(1);

            Assert.AreEqual(list.Count, 2);
        }

        [Test]
        public async Task JobsFinished2019_When_UserHasTasksThatFinishedAt2019_Then_ReturnJobsFinished2019()
        {
            var list = await service.JobsFinished2019(1);

            Assert.AreEqual(list.Count, 1);
        }

        [Test]
        public async Task Teams12yearsGroup_When_TeamsHaveUsersWithAgeMore12Years_Then_ReturnTeams12yearsGroup()
        {
            var list = await service.Teams12yearsGroup();

            Assert.AreEqual(list.Count, 0);
        }

        [Test]
        public async Task UsersACSJobsDSC_When_UsersHaveJobs_Then_ReturnUsersACSJobsDSC()
        {
            var list = await service.UsersACSJobsDSC();

            var expectedList = list.OrderBy(u => u.User.First_Name);

            foreach (var item in expectedList)
            {
                item.Jobs.OrderByDescending(j => j.Name.Length);
            }

            Assert.IsTrue(expectedList.SequenceEqual(list));
        }

        [Test]
        public async Task UserStruct_When_UserExisted_Then_ReturnUserStruct()
        {
            var userStruct = (await service.UserStruct(1))[0];

            var userStructExpected = new UserStruct()
            {
                User = new UserDTO()
                {
                    Id = 1,
                    First_Name = "Gayle1",
                    Last_Name = "Swift",
                    Email = "Gayle_Swift98@yahoo.com",
                    Birthday = new DateTime(2013, 11, 7, 13, 1, 47),
                    Registered_At = new DateTime(2019, 6, 17, 13, 1, 47),
                    Team_Id = 1,
                    Jobs = new List<JobDTO>()
                    {
                        new JobDTO()
                        {
                            Id = 1,
                            Name = "Eaque corporis illum ut.",
                            Description = "Inventore quae alias magnam sed qui sint velit est.\nSuscipit dolor ut omnis voluptate.\nExercitationem qui et temporibus dignissimos autem.\nSapiente recusandae illo maiores quasi.\nEt commodi harum voluptatem cum.",
                            Created_At = new DateTime(2019, 6, 16, 2, 54, 26),
                            Finished_At = new DateTime(2020, 2, 1, 17, 12, 48),
                            State = State.Started,
                            Performer_Id = 1,
                            Project_Id = 1
                        },
                        new JobDTO()
                        {
                            Id = 2,
                            Name = "Debitis quis ad quas voluptatem voluptatem.",
                            Description = "Similique molestiae esse eius nihil repudiandae possimus eos ea nobis.\nEaque ipsam atque sequi dignissimos fugiat voluptate hic nihil.\nNatus officia quis.\nDeleniti sit earum eveniet.\nCumque magni quis eaque atque natus expedita ratione illum.",
                            Created_At = new DateTime(2019, 6, 18, 19, 31, 15),
                            Finished_At = new DateTime(2019, 8, 9, 3, 39, 45),
                            State = State.Finished,
                            Performer_Id = 1,
                            Project_Id = 2
                        }
                    },
                    Projects = new List<ProjectDTO>()
                    {
                        new ProjectDTO()
                        {
                            Id = 1,
                            Name = "Rerum voluptatem beatae nesciunt consectetur.",
                            Description = "Ut quisquam molestias ut.\nQuia laudantium est quo asperiores veritatis porro ut quod doloribus.\nReiciendis odit ut hic libero.\nFuga debitis veniam ut.\nEos modi dolorem quia accusamus asperiores a et molestiae.\nAut eius et nihil voluptatem.",
                            Created_At = new DateTime(2019, 6, 18, 13, 46, 19),
                            Deadline = new DateTime(2020, 1, 13, 6, 24, 33),
                            Jobs = new List<JobDTO>()
                            {
                                new JobDTO()
                                {
                                    Id = 1,
                                    Name = "Eaque corporis illum ut.",
                                    Description = "Inventore quae alias magnam sed qui sint velit est.\nSuscipit dolor ut omnis voluptate.\nExercitationem qui et temporibus dignissimos autem.\nSapiente recusandae illo maiores quasi.\nEt commodi harum voluptatem cum.",
                                    Created_At = new DateTime(2019, 6, 16, 2, 54, 26),
                                    Finished_At = new DateTime(2020, 2, 1, 17, 12, 48),
                                    State = State.Started,
                                    Performer_Id = 1,
                                    Project_Id = 1
                                },
                                new JobDTO()
                                {
                                    Id = 5,
                                    Name = "Eos adipisci dignissimos minus non est est minus.",
                                    Description = "Possimus est asperiores repellendus tenetur placeat.\nDolor ipsa facere delectus tempore.\nUt eum esse et dolor delectus repudiandae vitae aut enim.\nDeleniti sint delectus saepe autem.",
                                    Created_At = new DateTime(2019, 6, 18, 21, 44, 18),
                                    Finished_At = new DateTime(2020, 1, 11, 8, 49, 6),
                                    State = State.Finished,
                                    Performer_Id = 3,
                                    Project_Id = 1
                                }
                            },
                            Author_Id = 1,
                            Team_Id = 2
                        }
                    }
                },
                LastProject = new ProjectDTO()
                {
                    Id = 1,
                    Name = "Rerum voluptatem beatae nesciunt consectetur.",
                    Description = "Ut quisquam molestias ut.\nQuia laudantium est quo asperiores veritatis porro ut quod doloribus.\nReiciendis odit ut hic libero.\nFuga debitis veniam ut.\nEos modi dolorem quia accusamus asperiores a et molestiae.\nAut eius et nihil voluptatem.",
                    Created_At = new DateTime(2019, 6, 18, 13, 46, 19),
                    Deadline = new DateTime(2020, 1, 13, 6, 24, 33),
                    Jobs = new List<JobDTO>()
                    {
                        new JobDTO()
                        {
                            Id = 1,
                            Name = "Eaque corporis illum ut.",
                            Description = "Inventore quae alias magnam sed qui sint velit est.\nSuscipit dolor ut omnis voluptate.\nExercitationem qui et temporibus dignissimos autem.\nSapiente recusandae illo maiores quasi.\nEt commodi harum voluptatem cum.",
                            Created_At = new DateTime(2019, 6, 16, 2, 54, 26),
                            Finished_At = new DateTime(2020, 2, 1, 17, 12, 48),
                            State = State.Started,
                            Performer_Id = 1,
                            Project_Id = 1
                        },
                        new JobDTO()
                        {
                            Id = 5,
                            Name = "Eos adipisci dignissimos minus non est est minus.",
                            Description = "Possimus est asperiores repellendus tenetur placeat.\nDolor ipsa facere delectus tempore.\nUt eum esse et dolor delectus repudiandae vitae aut enim.\nDeleniti sint delectus saepe autem.",
                            Created_At = new DateTime(2019, 6, 18, 21, 44, 18),
                            Finished_At = new DateTime(2020, 1, 11, 8, 49, 6),
                            State = State.Finished,
                            Performer_Id = 3,
                            Project_Id = 1
                        }
                    },
                    Author_Id = 1,
                    Team_Id = 2
                },
                LastProjectJobsCount = 2,
                NotFinishedCanceledJobs = new List<JobDTO>()
                {
                    new JobDTO()
                        {
                            Id = 1,
                            Name = "Eaque corporis illum ut.",
                            Description = "Inventore quae alias magnam sed qui sint velit est.\nSuscipit dolor ut omnis voluptate.\nExercitationem qui et temporibus dignissimos autem.\nSapiente recusandae illo maiores quasi.\nEt commodi harum voluptatem cum.",
                            Created_At = new DateTime(2019, 6, 16, 2, 54, 26),
                            Finished_At = new DateTime(2020, 2, 1, 17, 12, 48),
                            State = State.Started,
                            Performer_Id = 1,
                            Project_Id = 1
                        }
                },
                MaxDurationJob = new JobDTO()
                {
                    Id = 1,
                    Name = "Eaque corporis illum ut.",
                    Description = "Inventore quae alias magnam sed qui sint velit est.\nSuscipit dolor ut omnis voluptate.\nExercitationem qui et temporibus dignissimos autem.\nSapiente recusandae illo maiores quasi.\nEt commodi harum voluptatem cum.",
                    Created_At = new DateTime(2019, 6, 16, 2, 54, 26),
                    Finished_At = new DateTime(2020, 2, 1, 17, 12, 48),
                    State = State.Started,
                    Performer_Id = 1,
                    Project_Id = 1
                }
            };

            Assert.AreEqual(userStruct.User.Id, userStructExpected.User.Id);
            Assert.AreEqual(userStruct.User.Team_Id, userStructExpected.User.Team_Id);
            for (int i = 0; i < userStruct.User.Jobs.Count; i++)
            {
                Assert.AreEqual(userStruct.User.Jobs[i].Id, userStructExpected.User.Jobs[i].Id);
            }
            for (int i = 0; i < userStruct.User.Projects.Count; i++)
            {
                Assert.AreEqual(userStruct.User.Projects[i].Id, userStructExpected.User.Projects[i].Id);
            }
            Assert.AreEqual(userStruct.LastProjectJobsCount, userStructExpected.LastProjectJobsCount);
            for (int i = 0; i < userStruct.NotFinishedCanceledJobs.Count; i++)
            {
                Assert.AreEqual(userStruct.NotFinishedCanceledJobs[i].Id, userStructExpected.NotFinishedCanceledJobs[i].Id);
            }
            Assert.AreEqual(userStruct.MaxDurationJob.Id, userStructExpected.MaxDurationJob.Id);
        }

        [Test]
        public async Task ProjectStruct_When_ProjectExisted_Then_ReturnProjectStruct()
        {
            var projectStruct = (await service.ProjectStruct(1))[0];

            var projectStructExpected = new ProjectStruct()
            {
                Project = new ProjectDTO()
                {
                    Id = 1,
                    Name = "Rerum voluptatem beatae nesciunt consectetur.",
                    Description = "Ut quisquam molestias ut.\nQuia laudantium est quo asperiores veritatis porro ut quod doloribus.\nReiciendis odit ut hic libero.\nFuga debitis veniam ut.\nEos modi dolorem quia accusamus asperiores a et molestiae.\nAut eius et nihil voluptatem.",
                    Created_At = new DateTime(2019, 6, 18, 13, 46, 19),
                    Deadline = new DateTime(2020, 1, 13, 6, 24, 33),
                    Jobs = new List<JobDTO>()
                    {
                        new JobDTO()
                        {
                            Id = 1,
                            Name = "Eaque corporis illum ut.",
                            Description = "Inventore quae alias magnam sed qui sint velit est.\nSuscipit dolor ut omnis voluptate.\nExercitationem qui et temporibus dignissimos autem.\nSapiente recusandae illo maiores quasi.\nEt commodi harum voluptatem cum.",
                            Created_At = new DateTime(2019, 6, 16, 2, 54, 26),
                            Finished_At = new DateTime(2020, 2, 1, 17, 12, 48),
                            State = State.Started,
                            Performer_Id = 1,
                            Project_Id = 1
                        },
                        new JobDTO()
                        {
                            Id = 5,
                            Name = "Eos adipisci dignissimos minus non est est minus.",
                            Description = "Possimus est asperiores repellendus tenetur placeat.\nDolor ipsa facere delectus tempore.\nUt eum esse et dolor delectus repudiandae vitae aut enim.\nDeleniti sint delectus saepe autem.",
                            Created_At = new DateTime(2019, 6, 18, 21, 44, 18),
                            Finished_At = new DateTime(2020, 1, 11, 8, 49, 6),
                            State = State.Finished,
                            Performer_Id = 3,
                            Project_Id = 1
                        }
                    },
                    Author_Id = 1,
                    Team_Id = 2
                },
                LongestJob = new JobDTO()
                {
                    Id = 1,
                    Name = "Eaque corporis illum ut.",
                    Description = "Inventore quae alias magnam sed qui sint velit est.\nSuscipit dolor ut omnis voluptate.\nExercitationem qui et temporibus dignissimos autem.\nSapiente recusandae illo maiores quasi.\nEt commodi harum voluptatem cum.",
                    Created_At = new DateTime(2019, 6, 16, 2, 54, 26),
                    Finished_At = new DateTime(2020, 2, 1, 17, 12, 48),
                    State = State.Started,
                    Performer_Id = 1,
                    Project_Id = 1
                },
                ShortestJob = new JobDTO()
                {
                    Id = 1,
                    Name = "Eaque corporis illum ut.",
                    Description = "Inventore quae alias magnam sed qui sint velit est.\nSuscipit dolor ut omnis voluptate.\nExercitationem qui et temporibus dignissimos autem.\nSapiente recusandae illo maiores quasi.\nEt commodi harum voluptatem cum.",
                    Created_At = new DateTime(2019, 6, 16, 2, 54, 26),
                    Finished_At = new DateTime(2020, 2, 1, 17, 12, 48),
                    State = State.Started,
                    Performer_Id = 1,
                    Project_Id = 1
                },
                UsersCount = 1
            };

            Assert.AreEqual(projectStruct.Project.Id, projectStructExpected.Project.Id);
            Assert.AreEqual(projectStruct.LongestJob.Id, projectStructExpected.LongestJob.Id);
            Assert.AreEqual(projectStruct.ShortestJob.Id, projectStructExpected.ShortestJob.Id);
            Assert.AreEqual(projectStruct.UsersCount, projectStructExpected.UsersCount);
        }
    }
}