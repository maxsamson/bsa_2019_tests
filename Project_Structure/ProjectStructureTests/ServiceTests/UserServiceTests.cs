﻿using DAL.Models;
using FluentValidation;
using Microsoft.Extensions.Configuration;
using Moq;
using NUnit.Framework;
using Project_Structure.BLL.Hubs;
using Project_Structure.BLL.Interfaces;
using Project_Structure.BLL.Mapping;
using Project_Structure.BLL.Services;
using Project_Structure.BLL.Validators;
using Project_Structure.Shared.DTO;
using ProjectStructureTests.Fake;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructureTests.ServiceTests
{
    public class UserServiceTests
    {
        private readonly FakeRepository<User> userRepo;

        private readonly IMapper mapper;
        private readonly AbstractValidator<UserDTO> validator;
        private readonly UserService service;

        public UserServiceTests()
        {
            userRepo = new FakeRepository<User>();

            mapper = new Mapper(new FakeRepository<Project>(), new FakeRepository<Job>(), new FakeRepository<Team>(), userRepo);
            validator = new UserValidator();
            service = new UserService(userRepo, mapper, validator, 
                new Mock<QueueService>(new Mock<IConfiguration>().Object, new Mock<ProjectHub>().Object).Object);
        }

        [SetUp]
        public void TestSetup()
        {

        }

        [Test]
        public async Task Create_When_UserNotExisted_Then_AppearsInCollection()
        {
            var user = new UserDTO
            {
                Id = 1,
                First_Name = "Gayle1",
                Last_Name = "Swift",
                Email = "Gayle_Swift98@yahoo.com",
                Birthday = new DateTime(2013, 11, 7, 13, 1, 47),
                Registered_At = new DateTime(2019, 6, 17, 13, 1, 47)
            };

            var id = await service.Create(user);

            Assert.AreEqual(userRepo.Collection.Count, 1);
            Assert.AreEqual(userRepo.Collection[0].Id, id);

            Assert.AreEqual(userRepo.Collection[0].Id, user.Id);
            Assert.AreEqual(userRepo.Collection[0].First_Name, user.First_Name);
            Assert.AreEqual(userRepo.Collection[0].Last_Name, user.Last_Name);
            Assert.AreEqual(userRepo.Collection[0].Email, user.Email);
            Assert.AreEqual(userRepo.Collection[0].Birthday, user.Birthday);
            Assert.AreEqual(userRepo.Collection[0].Registered_At, user.Registered_At);
        }
    }
}
