﻿using DAL.DBInfrastructure;
using DAL.Models;
using DAL.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Moq;
using Newtonsoft.Json;
using NUnit.Framework;
using Project_Structure.BLL.Hubs;
using Project_Structure.BLL.Mapping;
using Project_Structure.BLL.Services;
using Project_Structure.Controllers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructureTests.APITests
{
    public class SelectionControllerTests
    {
        readonly ProjectDbContext context;
        readonly SelectionController controller;

        public SelectionControllerTests()
        {
            var options = new DbContextOptionsBuilder<ProjectDbContext>()
                .UseInMemoryDatabase(databaseName: "123")
                .Options;

            context = new ProjectDbContext(options);

            var projRepo = new ProjectRepository(context);
            var jobRepo = new JobRepository(context);
            var teamRepo = new TeamRepository(context);
            var userRepo = new UserRepository(context);

            var mapper = new Mapper(projRepo, jobRepo, teamRepo, userRepo);

            var queueService = new Mock<QueueService>(new Mock<IConfiguration>().Object, new Mock<ProjectHub>().Object).Object;

            var service = new SelectionService(projRepo, jobRepo, teamRepo, userRepo, mapper, queueService);

            controller = new SelectionController(service, new Mock<IConfiguration>().Object);
        }

        [SetUp]
        public void TestSetup()
        {

        }

        [Test]
        public async Task JobsWithNameLess45Symbols_When_UserHasTasksWithNameLess45Symbols_Then_ReturnJobsWithNameLess45Symbols()
        {
            var result = (await controller.JobsWithNameLess45Symbols(1)) as OkObjectResult;

            var list = JsonConvert.DeserializeObject<List<Job>>(JsonConvert.SerializeObject(result.Value), 
                new JsonSerializerSettings
                {
                    DateFormatString = "YYYY-MM-DDTHH:mm:ss.FFFZ"
                });

            Assert.AreEqual(list.Count, 2);
            Assert.AreEqual(list[0].Id, 1);
            Assert.AreEqual(list[1].Id, 2);
        }
    }
}
