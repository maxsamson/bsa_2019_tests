﻿using DAL.DBInfrastructure;
using DAL.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Moq;
using NUnit.Framework;
using Project_Structure.BLL.Hubs;
using Project_Structure.BLL.Mapping;
using Project_Structure.BLL.Services;
using Project_Structure.BLL.Validators;
using Project_Structure.Controllers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructureTests.APITests
{
    public class UsersControllerTests
    {
        readonly ProjectDbContext context;
        readonly UsersController controller;

        public UsersControllerTests()
        {
            var options = new DbContextOptionsBuilder<ProjectDbContext>()
                .UseInMemoryDatabase(databaseName: "123")
                .Options;

            context = new ProjectDbContext(options);

            var projRepo = new ProjectRepository(context);
            var jobRepo = new JobRepository(context);
            var teamRepo = new TeamRepository(context);
            var userRepo = new UserRepository(context);

            var mapper = new Mapper(projRepo, jobRepo, teamRepo, userRepo);

            var validator = new UserValidator();

            var queueService = new Mock<QueueService>(new Mock<IConfiguration>().Object, new Mock<ProjectHub>().Object).Object;

            var service = new UserService(userRepo, mapper, validator, queueService);

            controller = new UsersController(service);
        }

        [SetUp]
        public void TestSetup()
        {

        }

        [Test]
        public async Task Delete_When_UserExisted_Then_DisappearsFromDatabase()
        {
            int id = 2;

            var result = (await controller.Delete(id)) as OkObjectResult;

            Assert.AreEqual(result.StatusCode, 200);
            Assert.IsNull(await context.Users.SingleOrDefaultAsync(u => u.Id == id));
        }
    }
}
