﻿using DAL.DBInfrastructure;
using DAL.Models;
using DAL.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Moq;
using NUnit.Framework;
using Project_Structure.BLL.Hubs;
using Project_Structure.BLL.Mapping;
using Project_Structure.BLL.Services;
using Project_Structure.BLL.Validators;
using Project_Structure.Controllers;
using Project_Structure.Shared.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructureTests.APITests
{
    public class TeamsControllerTests
    {
        readonly ProjectDbContext context;
        readonly TeamsController controller;

        public TeamsControllerTests()
        {
            var options = new DbContextOptionsBuilder<ProjectDbContext>()
                .UseInMemoryDatabase(databaseName: "123")
                .Options;

            context = new ProjectDbContext(options);

            var projRepo = new ProjectRepository(context);
            var jobRepo = new JobRepository(context);
            var teamRepo = new TeamRepository(context);
            var userRepo = new UserRepository(context);

            var mapper = new Mapper(projRepo, jobRepo, teamRepo, userRepo);

            var validator = new TeamValidator();

            var queueService = new Mock<QueueService>(new Mock<IConfiguration>().Object, new Mock<ProjectHub>().Object).Object;

            var service = new TeamService(teamRepo, mapper, validator, queueService);

            controller = new TeamsController(service);
        }

        [SetUp]
        public void TestSetup()
        {

        }

        [Test]
        public async Task Post_When_TeamNotExisted_Then_AppearsInDatabase()
        {
            var team = new TeamDTO
            {
                Name = "123",
                Created_At = new DateTime(2019, 6, 19, 1, 0, 24),
                Projects = new List<ProjectDTO>()
                {
                    new ProjectDTO()
                        {
                            Id = 1,
                            Name = "Rerum voluptatem beatae nesciunt consectetur.",
                            Description = "Ut quisquam molestias ut.\nQuia laudantium est quo asperiores veritatis porro ut quod doloribus.\nReiciendis odit ut hic libero.\nFuga debitis veniam ut.\nEos modi dolorem quia accusamus asperiores a et molestiae.\nAut eius et nihil voluptatem.",
                            Created_At = new DateTime(2019, 6, 18, 13, 46, 19),
                            Deadline = new DateTime(2020, 1, 13, 6, 24, 33),
                            Jobs = new List<JobDTO>()
                            {
                                new JobDTO()
                                {
                                    Name = "Eaque corporis illum ut.",
                                    Description = "Inventore quae alias magnam sed qui sint velit est.\nSuscipit dolor ut omnis voluptate.\nExercitationem qui et temporibus dignissimos autem.\nSapiente recusandae illo maiores quasi.\nEt commodi harum voluptatem cum.",
                                    Created_At = new DateTime(2019, 6, 16, 2, 54, 26),
                                    Finished_At = new DateTime(2020, 2, 1, 17, 12, 48),
                                    State = State.Started,
                                    Performer_Id = 1,
                                    Project_Id = 1
                                }
                            },
                            Author_Id = 1,
                        }
                },
                Users = new List<UserDTO>()
                {
                    new UserDTO()
                    {
                        First_Name = "Gayle1",
                        Last_Name = "Swift",
                        Email = "Gayle_Swift98@yahoo.com",
                        Birthday = new DateTime(2013, 11, 7, 13, 1, 47),
                        Registered_At = new DateTime(2019, 6, 17, 13, 1, 47),
                        Team_Id = 1,
                        Jobs = new List<JobDTO>()
                        {
                            new JobDTO()
                            {
                                Name = "Eaque corporis illum ut.",
                                Description = "Inventore quae alias magnam sed qui sint velit est.\nSuscipit dolor ut omnis voluptate.\nExercitationem qui et temporibus dignissimos autem.\nSapiente recusandae illo maiores quasi.\nEt commodi harum voluptatem cum.",
                                Created_At = new DateTime(2019, 6, 16, 2, 54, 26),
                                Finished_At = new DateTime(2020, 2, 1, 17, 12, 48),
                                State = State.Started,
                                Performer_Id = 1,
                                Project_Id = 1
                            },
                            new JobDTO()
                            {
                                Name = "Debitis quis ad quas voluptatem voluptatem.",
                                Description = "Similique molestiae esse eius nihil repudiandae possimus eos ea nobis.\nEaque ipsam atque sequi dignissimos fugiat voluptate hic nihil.\nNatus officia quis.\nDeleniti sit earum eveniet.\nCumque magni quis eaque atque natus expedita ratione illum.",
                                Created_At = new DateTime(2019, 6, 18, 19, 31, 15),
                                Finished_At = new DateTime(2019, 8, 9, 3, 39, 45),
                                State = State.Finished,
                                Performer_Id = 1,
                                Project_Id = 2
                            }
                        },
                        Projects = new List<ProjectDTO>()
                        {
                            new ProjectDTO()
                            {
                                Name = "Rerum voluptatem beatae nesciunt consectetur.",
                                Description = "Ut quisquam molestias ut.\nQuia laudantium est quo asperiores veritatis porro ut quod doloribus.\nReiciendis odit ut hic libero.\nFuga debitis veniam ut.\nEos modi dolorem quia accusamus asperiores a et molestiae.\nAut eius et nihil voluptatem.",
                                Created_At = new DateTime(2019, 6, 18, 13, 46, 19),
                                Deadline = new DateTime(2020, 1, 13, 6, 24, 33),
                                Jobs = new List<JobDTO>()
                                {
                                    new JobDTO()
                                    {
                                        Id = 1,
                                        Name = "Eaque corporis illum ut.",
                                        Description = "Inventore quae alias magnam sed qui sint velit est.\nSuscipit dolor ut omnis voluptate.\nExercitationem qui et temporibus dignissimos autem.\nSapiente recusandae illo maiores quasi.\nEt commodi harum voluptatem cum.",
                                        Created_At = new DateTime(2019, 6, 16, 2, 54, 26),
                                        Finished_At = new DateTime(2020, 2, 1, 17, 12, 48),
                                        State = State.Started,
                                        Performer_Id = 1,
                                        Project_Id = 1
                                    },
                                    new JobDTO()
                                    {
                                        Id = 5,
                                        Name = "Eos adipisci dignissimos minus non est est minus.",
                                        Description = "Possimus est asperiores repellendus tenetur placeat.\nDolor ipsa facere delectus tempore.\nUt eum esse et dolor delectus repudiandae vitae aut enim.\nDeleniti sint delectus saepe autem.",
                                        Created_At = new DateTime(2019, 6, 18, 21, 44, 18),
                                        Finished_At = new DateTime(2020, 1, 11, 8, 49, 6),
                                        State = State.Finished,
                                        Performer_Id = 3,
                                        Project_Id = 1
                                    }
                                },
                                Author_Id = 1,
                                Team_Id = 2
                            }
                        }
                    },
                    new UserDTO()
                    {
                        First_Name = "Gayle1",
                        Last_Name = "Swift",
                        Email = "Gayle_Swift98@yahoo.com",
                        Birthday = new DateTime(2013, 11, 7, 13, 1, 47),
                        Registered_At = new DateTime(2019, 6, 17, 13, 1, 47),
                        Team_Id = 1,
                        Jobs = new List<JobDTO>()
                        {
                            new JobDTO()
                            {
                                Name = "333333333333333",
                                Description = "Inventore quae alias magnam sed qui sint velit est.\nSuscipit dolor ut omnis voluptate.\nExercitationem qui et temporibus dignissimos autem.\nSapiente recusandae illo maiores quasi.\nEt commodi harum voluptatem cum.",
                                Created_At = new DateTime(2019, 6, 16, 2, 54, 26),
                                Finished_At = new DateTime(2020, 2, 1, 17, 12, 48),
                                State = State.Started,
                                Performer_Id = 1,
                                Project_Id = 1
                            },
                            new JobDTO()
                            {
                                Name = "444444444444444444444",
                                Description = "Similique molestiae esse eius nihil repudiandae possimus eos ea nobis.\nEaque ipsam atque sequi dignissimos fugiat voluptate hic nihil.\nNatus officia quis.\nDeleniti sit earum eveniet.\nCumque magni quis eaque atque natus expedita ratione illum.",
                                Created_At = new DateTime(2019, 6, 18, 19, 31, 15),
                                Finished_At = new DateTime(2019, 8, 9, 3, 39, 45),
                                State = State.Finished,
                                Performer_Id = 1,
                                Project_Id = 2
                            }
                        },
                        Projects = new List<ProjectDTO>()
                        {
                            new ProjectDTO()
                            {
                                Name = "222222222222222222",
                                Description = "Ut quisquam molestias ut.\nQuia laudantium est quo asperiores veritatis porro ut quod doloribus.\nReiciendis odit ut hic libero.\nFuga debitis veniam ut.\nEos modi dolorem quia accusamus asperiores a et molestiae.\nAut eius et nihil voluptatem.",
                                Created_At = new DateTime(2019, 6, 18, 13, 46, 19),
                                Deadline = new DateTime(2020, 1, 13, 6, 24, 33),
                                Jobs = new List<JobDTO>()
                                {
                                    new JobDTO()
                                    {
                                        Name = "22222222222222222",
                                        Description = "Inventore quae alias magnam sed qui sint velit est.\nSuscipit dolor ut omnis voluptate.\nExercitationem qui et temporibus dignissimos autem.\nSapiente recusandae illo maiores quasi.\nEt commodi harum voluptatem cum.",
                                        Created_At = new DateTime(2019, 6, 16, 2, 54, 26),
                                        Finished_At = new DateTime(2020, 2, 1, 17, 12, 48),
                                        State = State.Started,
                                        Performer_Id = 1,
                                        Project_Id = 1
                                    },
                                    new JobDTO()
                                    {
                                        Name = "4444444444444444444444",
                                        Description = "Possimus est asperiores repellendus tenetur placeat.\nDolor ipsa facere delectus tempore.\nUt eum esse et dolor delectus repudiandae vitae aut enim.\nDeleniti sint delectus saepe autem.",
                                        Created_At = new DateTime(2019, 6, 18, 21, 44, 18),
                                        Finished_At = new DateTime(2020, 1, 11, 8, 49, 6),
                                        State = State.Finished,
                                        Performer_Id = 3,
                                        Project_Id = 1
                                    }
                                },
                                Author_Id = 1,
                                Team_Id = 2
                            }
                        }
                    }
                }
            };

            var result = (await controller.Post(team)) as OkObjectResult;

            var id = Convert.ToInt32(result.Value);

            var expectedResult = await context.Teams.SingleAsync(p => p.Id == id);

            Assert.AreEqual(expectedResult.Name, team.Name);
        }
    }
}
