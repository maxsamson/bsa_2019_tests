﻿using DAL.DBInfrastructure;
using DAL.Models;
using DAL.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Moq;
using NUnit.Framework;
using Project_Structure.BLL.Hubs;
using Project_Structure.BLL.Interfaces;
using Project_Structure.BLL.Mapping;
using Project_Structure.BLL.Services;
using Project_Structure.BLL.Validators;
using Project_Structure.Controllers;
using Project_Structure.Shared.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructureTests.APITests
{
    public class ProjectControllerTests
    {
        readonly ProjectDbContext context;
        readonly ProjectsController controller;

        public ProjectControllerTests()
        {
            var options = new DbContextOptionsBuilder<ProjectDbContext>()
                .UseInMemoryDatabase(databaseName: "123")
                .Options;

            context = new ProjectDbContext(options);

            var projRepo = new ProjectRepository(context);
            var jobRepo = new JobRepository(context);
            var teamRepo = new TeamRepository(context);
            var userRepo = new UserRepository(context);

            var mapper = new Mapper(projRepo, jobRepo, teamRepo, userRepo);

            var validator = new ProjectValidator();

            var queueService = new Mock<QueueService>(new Mock<IConfiguration>().Object, new Mock<ProjectHub>().Object).Object;

            var service = new ProjectService(projRepo, mapper, validator, queueService);

            controller = new ProjectsController(service);
        }

        [SetUp]
        public void TestSetup()
        {

        }

        [Test]
        public async Task Post_When_ProjectNotExisted_Then_AppearsInDatabase()
        {
            var project = new ProjectDTO()
            {
                Name = "123",
                Description = "321",
                Created_At = new DateTime(2019, 6, 18, 13, 46, 19),
                Deadline = new DateTime(2020, 1, 13, 6, 24, 33),
                Jobs = new List<JobDTO>()
                    {
                        new JobDTO()
                        {
                            Id = 1,
                            Name = "Eaque corporis illum ut.",
                            Description = "Inventore quae alias magnam sed qui sint velit est.\nSuscipit dolor ut omnis voluptate.\nExercitationem qui et temporibus dignissimos autem.\nSapiente recusandae illo maiores quasi.\nEt commodi harum voluptatem cum.",
                            Created_At = new DateTime(2019, 6, 16, 2, 54, 26),
                            Finished_At = new DateTime(2020, 2, 1, 17, 12, 48),
                            State = State.Started,
                            Performer_Id = 1,
                            Project_Id = 1
                        },
                        new JobDTO()
                        {
                            Id = 5,
                            Name = "Eos adipisci dignissimos minus non est est minus.",
                            Description = "Possimus est asperiores repellendus tenetur placeat.\nDolor ipsa facere delectus tempore.\nUt eum esse et dolor delectus repudiandae vitae aut enim.\nDeleniti sint delectus saepe autem.",
                            Created_At = new DateTime(2019, 6, 18, 21, 44, 18),
                            Finished_At = new DateTime(2020, 1, 11, 8, 49, 6),
                            State = State.Finished,
                            Performer_Id = 3,
                            Project_Id = 1
                        }
                    },
                Author_Id = 1,
                Team_Id = 2
            };

            var result = (await controller.Post(project)) as OkObjectResult;

            var id = Convert.ToInt32(result.Value);

            var expectedResult = await context.Projects.SingleAsync(p => p.Id == id);

            Assert.AreEqual(expectedResult.Name, project.Name);
        }
    }
}
