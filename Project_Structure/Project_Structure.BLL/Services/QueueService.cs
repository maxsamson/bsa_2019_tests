﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using Project_Structure.BLL.Hubs;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Structure.BLL.Services
{
    public class QueueService
    {
        private readonly IConfiguration config;
        private readonly ProjectHub hub;

        private IConnection connection;
        private IModel channel;
        EventingBasicConsumer consumer;

        public QueueService(IConfiguration config, ProjectHub hub)
        {
            this.config = config;
            this.hub = hub;

            //ConfigureBackQueue();
        }

        public bool PostValueWorker(string message)
        {
            var factory = new ConnectionFactory()
            {
                Uri = new Uri(config.GetSection("Rabbit").Value)
            };

            channel.ExchangeDeclare("WorkerExchange", ExchangeType.Direct);

            var body = Encoding.UTF8.GetBytes(message);

            channel.BasicPublish(
                exchange: "WorkerExchange",
                routingKey: "worker",
                basicProperties: null,
                body: body);

            return true;
        }

        private void ConfigureBackQueue()
        {
            var factory = new ConnectionFactory
            {
                Uri = new Uri(config["Rabbit"])
            };

            connection = factory.CreateConnection();
            channel = connection.CreateModel();
            channel.ExchangeDeclare("WorkerExchange", ExchangeType.Direct);
            channel.QueueDeclare(
                queue: "ServerQueue",
                durable: true,
                exclusive: false,
                autoDelete: false);
            channel.QueueBind("ServerQueue", "WorkerExchange", "server");

            consumer = new EventingBasicConsumer(channel);
            consumer.Received += PostValueClient;

            channel.BasicConsume(
                queue: "ServerQueue",
                autoAck: false,
                consumer: consumer);
        }

        private void PostValueClient(object sender, BasicDeliverEventArgs args)
        {
            hub.Clients.All.SendAsync("PostResult", Encoding.UTF8.GetString(args.Body));

            channel.BasicAck(args.DeliveryTag, false);
        }
    }
}
