﻿using Project_Structure.Shared.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Project_Structure.BLL.Interfaces
{
    public interface ITeamService
    {
        Task<List<TeamDTO>> GetAll();

        Task<TeamDTO> GetById(int id);

        Task<int> Create(TeamDTO team);

        Task Update(int id, TeamDTO team);

        Task Delete(TeamDTO team);

        Task DeleteById(int id);
    }
}
