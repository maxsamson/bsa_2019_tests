﻿using Project_Structure.Shared.DTO;
using Project_Structure.Shared.SelectionModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Project_Structure.BLL.Interfaces
{
    public interface ISelectionService
    {
        Task<List<ProjectDTO>> CreateHierarhy();

        Task<List<JobsCount>> JobsCount(int user_Id);

        Task<List<JobDTO>> JobsWithNameLess45Symbols(int user_Id);

        Task<List<JobsFinished2019>> JobsFinished2019(int user_Id);

        Task<List<Teams12yearsGroup>> Teams12yearsGroup();

        Task<List<UsersACSJobsDSC>> UsersACSJobsDSC();

        Task<List<UserStruct>> UserStruct(int user_Id);

        Task<List<ProjectStruct>> ProjectStruct(int proj_Id);

        Task<List<Message>> GetMessages(string path);
    }
}
