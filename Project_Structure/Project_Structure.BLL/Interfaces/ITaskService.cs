﻿using Project_Structure.Shared.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Project_Structure.BLL.Interfaces
{
    public interface IJobService
    {
        Task<List<JobDTO>> GetAll();

        Task<JobDTO> GetById(int id);

        Task<int> Create(JobDTO Job);

        Task Update(int id, JobDTO Job);

        Task Delete(JobDTO Job);

        Task DeleteById(int id);
    }
}
