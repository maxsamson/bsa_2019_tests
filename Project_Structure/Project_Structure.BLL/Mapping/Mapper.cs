﻿using DAL.Interfaces;
using DAL.Models;
using DAL.Repositories;
using Project_Structure.BLL.Interfaces;
using Project_Structure.Shared.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Project_Structure.BLL.Mapping
{
    public class Mapper : IMapper
    {
        private readonly IRepository<Project> projectRepo;
        private readonly IRepository<Job> JobRepo;
        private readonly IRepository<Team> teamRepo;
        private readonly IRepository<User> userRepo;

        public Mapper(IRepository<Project> projectRepo, IRepository<Job> JobRepo, IRepository<Team> teamRepo, IRepository<User> userRepo)
        {
            this.projectRepo = projectRepo;
            this.JobRepo = JobRepo;
            this.teamRepo = teamRepo;
            this.userRepo = userRepo;
        }

        public ProjectDTO MapProject(Project value)
        {
            var listJobs = new List<JobDTO>();
            if (value.Jobs != null)
            {
                foreach (var item in value.Jobs)
                    listJobs.Add(MapJob(item));
            }

            return new ProjectDTO()
            {
                Id = value.Id,
                Name = value.Name,
                Description = value.Description,
                Created_At = value.Created_At,
                Deadline = value.Deadline,
                Jobs = listJobs,
                Author_Id = value.Author.Id,
                Team_Id = value.Team.Id
            };
        }

        public async Task<Project> MapProject(ProjectDTO value)
        {
            var listJobs = await JobRepo.GetAll();
            var listUserJobs = new List<Job>();
            if (value.Jobs != null)
            {
                foreach (var item in value.Jobs)
                {
                    bool check = false;
                    foreach (var Job in listJobs)
                    {
                        if (item.Id == Job.Id)
                        {
                            listUserJobs.Add(Job);
                            check = true;
                        }
                        if (check)
                            break;
                    }
                }
            }

            return new Project()
            {
                Id = value.Id,
                Name = value.Name,
                Description = value.Description,
                Created_At = value.Created_At,
                Deadline = value.Deadline,
                Jobs = listUserJobs,
                Author = await userRepo.GetById(value.Author_Id),
                Team = await teamRepo.GetById(value.Team_Id)
                //Author = (await projectRepo.GetById(value.Id)).Author,
                //Team = (await projectRepo.GetById(value.Id)).Team
            };
        }

        public List<ProjectDTO> MapProjects(List<Project> valueList)
        {
            List<ProjectDTO> resultList = new List<ProjectDTO>();

            foreach (var item in valueList)
                resultList.Add(MapProject(item));

            return resultList;
        }

        public async Task<List<Project>> MapProjects(List<ProjectDTO> valueList)
        {
            List<Project> resultList = new List<Project>();

            foreach (var item in valueList)
                resultList.Add(await MapProject(item));

            return resultList;
        }

        public JobDTO MapJob(Job value)
        {
            return new JobDTO
            {
                Id = value.Id,
                Name = value.Name,
                Description = value.Description,
                Created_At = value.Created_At,
                Finished_At = value.Finished_At,
                State = value.State,
                Project_Id = value.Project.Id,
                Performer_Id = value.User.Id
            };
        }

        public async Task<Job> MapJob(JobDTO value)
        {
            return new Job
            {
                Id = value.Id,
                Name = value.Name,
                Description = value.Description,
                Created_At = value.Created_At,
                Finished_At = value.Finished_At,
                State = value.State,
                Project = (await JobRepo.GetById(value.Id)).Project,
                User = (await JobRepo.GetById(value.Id)).User
            };
        }

        public List<JobDTO> MapJobs(List<Job> valueList)
        {
            List<JobDTO> resultList = new List<JobDTO>();

            foreach (var item in valueList)
                resultList.Add(MapJob(item));

            return resultList;
        }

        public async Task<List<Job>> MapJobs(List<JobDTO> valueList)
        {
            List<Job> resultList = new List<Job>();

            foreach (var item in valueList)
                resultList.Add(await MapJob(item));

            return resultList;
        }

        public TeamDTO MapTeam(Team value)
        {
            var listUsers = new List<UserDTO>();
            if (value.Users != null)
            {
                foreach (var item in value.Users)
                    listUsers.Add(MapUser(item));
            }

            var listProjects = new List<ProjectDTO>();
            if (value.Projects != null)
            {
                foreach (var item in value.Projects)
                    listProjects.Add(MapProject(item));
            }

            return new TeamDTO
            {
                Id = value.Id,
                Name = value.Name,
                Created_At = value.Created_At,
                Users = listUsers,
                Projects = listProjects
            };
        }

        public async Task<Team> MapTeam(TeamDTO value)
        {
            var listUsers = await userRepo.GetAll();
            var listTeamUsers = new List<User>();
            if (value.Users != null)
            {
                foreach (var item in value.Users)
                {
                    bool check = false;
                    foreach (var user in listUsers)
                    {
                        if (item.Id == user.Id)
                        {
                            listTeamUsers.Add(user);
                            check = true;
                        }
                        if (check)
                            break;
                    }
                }
            }

            var listProjects = await projectRepo.GetAll();
            var listTeamProjects = new List<Project>();
            if (value.Projects != null)
            {
                foreach (var item in value.Projects)
                {
                    bool check = false;
                    foreach (var project in listProjects)
                    {
                        if (item.Id == project.Id)
                        {
                            listTeamProjects.Add(project);
                            check = true;
                        }
                        if (check)
                            break;
                    }
                }
            }

            return new Team
            {
                Id = value.Id,
                Name = value.Name,
                Created_At = value.Created_At,
                Users = listTeamUsers,
                Projects = listTeamProjects
            };
        }

        public List<TeamDTO> MapTeams(List<Team> valueList)
        {
            List<TeamDTO> resultList = new List<TeamDTO>();

            foreach (var item in valueList)
                resultList.Add(MapTeam(item));

            return resultList;
        }

        public async Task<List<Team>> MapTeams(List<TeamDTO> valueList)
        {
            List<Team> resultList = new List<Team>();

            foreach (var item in valueList)
                resultList.Add(await MapTeam(item));

            return resultList;
        }

        public UserDTO MapUser(User value)
        {
            var listJobs = new List<JobDTO>();
            if (value.Jobs != null)
            {
                foreach (var item in value.Jobs)
                    listJobs.Add(MapJob(item));
            }

            var listProjects = new List<ProjectDTO>();
            if (value.Projects != null)
            {
                foreach (var item in value.Projects)
                    listProjects.Add(MapProject(item));
            }

            return new UserDTO
            {
                Id = value.Id,
                First_Name = value.First_Name,
                Last_Name = value.Last_Name,
                Email = value.Email,
                Birthday = value.Birthday,
                Registered_At = value.Registered_At,
                Jobs = listJobs,
                Projects = listProjects,
                Team_Id = value.Team.Id
            };
        }

        public async Task<User> MapUser(UserDTO value)
        {
            var listJobs = await JobRepo.GetAll();
            var listUserJobs = new List<Job>();
            if (value.Jobs != null)
            {
                foreach (var item in value.Jobs)
                {
                    bool check = false;
                    foreach (var Job in listJobs)
                    {
                        if (item.Id == Job.Id)
                        {
                            listUserJobs.Add(Job);
                            check = true;
                        }
                        if (check)
                            break;
                    }
                }
            }

            var listProjects = await projectRepo.GetAll();
            var listUserProjects = new List<Project>();
            if (value.Projects != null)
            {
                foreach (var item in value.Projects)
                {
                    bool check = false;
                    foreach (var project in listProjects)
                    {
                        if (item.Id == project.Id)
                        {
                            listUserProjects.Add(project);
                            check = true;
                        }
                        if (check)
                            break;
                    }
                }
            }

            return new User
            {
                Id = value.Id,
                First_Name = value.First_Name,
                Last_Name = value.Last_Name,
                Email = value.Email,
                Birthday = value.Birthday,
                Registered_At = value.Registered_At,
                Jobs = listUserJobs,
                Projects = listUserProjects,
                Team = (await userRepo.GetById(value.Id))?.Team
            };
        }

        public List<UserDTO> MapUsers(List<User> valueList)
        {
            List<UserDTO> resultList = new List<UserDTO>();

            foreach (var item in valueList)
                resultList.Add(MapUser(item));

            return resultList;
        }

        public async Task<List<User>> MapUsers(List<UserDTO> valueList)
        {
            List<User> resultList = new List<User>();

            foreach (var item in valueList)
                resultList.Add(await MapUser(item));

            return resultList;
        }
    }
}
