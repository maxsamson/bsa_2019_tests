﻿using Project_Structure.Shared.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Structure.Shared.SelectionModels
{
    public class Teams12yearsGroup
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<UserDTO> Users { get; set; }
    }
}
