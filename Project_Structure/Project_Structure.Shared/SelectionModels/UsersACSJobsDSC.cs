﻿using Project_Structure.Shared.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Structure.Shared.SelectionModels
{
    public class UsersACSJobsDSC
    {
        public UserDTO User { get; set; }
        public List<JobDTO> Jobs { get; set; }
    }
}
