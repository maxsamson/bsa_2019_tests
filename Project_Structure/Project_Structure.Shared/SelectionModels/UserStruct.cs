﻿using Project_Structure.Shared.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Structure.Shared.SelectionModels
{
    public class UserStruct
    {
        public UserDTO User { get; set; }
        public ProjectDTO LastProject { get; set; }
        public int LastProjectJobsCount { get; set; }
        public List<JobDTO> NotFinishedCanceledJobs { get; set; }
        public JobDTO MaxDurationJob { get; set; }
    }
}
