﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Structure.Shared.DTO
{
    public class TeamDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Created_At { get; set; }
        public List<UserDTO> Users { get; set; }
        public List<ProjectDTO> Projects { get; set; }

        public TeamDTO()
        {
            Users = new List<UserDTO>();
            Projects = new List<ProjectDTO>();
        }
    }
}
