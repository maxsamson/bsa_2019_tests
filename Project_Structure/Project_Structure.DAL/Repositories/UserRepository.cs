﻿using DAL.DBInfrastructure;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Repositories
{
    public class UserRepository : BaseRepository<User>
    {
        public UserRepository(ProjectDbContext dBContext) : base(dBContext)
        {
            
        }
    }
}
