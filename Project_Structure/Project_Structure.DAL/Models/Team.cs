﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DAL.Models
{
    public class Team : Entity
    {
        [Required]
        [MaxLength(200)]
        public string Name { get; set; }
        [Required]
        public DateTime Created_At { get; set; }
        public List<User> Users { get; set; }
        public List<Project> Projects { get; set; }
    }
}
