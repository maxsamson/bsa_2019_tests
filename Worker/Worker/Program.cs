﻿using Microsoft.Extensions.Configuration;
using System;
using System.IO;

namespace Worker
{
    class Program
    {
        static void Main(string[] args)
        {
            IConfiguration config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", true, true)
                .Build();

            MessageService ms = new MessageService(config);

            ms.Run();

            while(true)
            {

            }
        }
    }
}
