﻿using Microsoft.Extensions.Configuration;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.IO;
using System.Text;

namespace Worker
{
    public class MessageService
    {
        private const string path = "..\\..\\..\\..\\..\\AppData";
        private const string fileName = "log.txt";

        private readonly IConfiguration config;

        private IConnection connection;
        private IModel channel;
        EventingBasicConsumer consumer;

        public MessageService(IConfiguration config)
        {
            this.config = config;
        }

        public void Run()
        {
            Console.WriteLine("Service run.");

            DirectoryInfo dirInfo = new DirectoryInfo(path);
            if (!dirInfo.Exists)
            {
                dirInfo.Create();
            }

            Configure();
        }

        public void Configure()
        {
            var factory = new ConnectionFactory
            {
                Uri = new Uri("amqp://guest:guest@localhost:5672")
            };

            connection = factory.CreateConnection();
            channel = connection.CreateModel();
            channel.ExchangeDeclare("WorkerExchange", ExchangeType.Direct);
            channel.QueueDeclare(
                queue: "WorkerQueue",
                durable: true,
                exclusive: false,
                autoDelete: false);
            channel.QueueBind("WorkerQueue", "WorkerExchange", "worker");

            consumer = new EventingBasicConsumer(channel);
            consumer.Received += WriteToFileAndCallBack;

            channel.BasicConsume(
                queue: "WorkerQueue",
                autoAck: false,
                consumer: consumer);
        }

        public void WriteToFileAndCallBack(object sender, BasicDeliverEventArgs args)
        {
            try
            {
                using (StreamWriter sw = new StreamWriter(path + "\\" + fileName, true, Encoding.Default))
                {
                    sw.WriteLine($"{DateTime.Now}\t{Encoding.UTF8.GetString(args.Body)}");
                }

                Console.WriteLine("Successfully wrote to file");

                channel.BasicAck(args.DeliveryTag, false);

                var body = Encoding.UTF8.GetBytes("Successfully received");

                channel.BasicPublish(
                    exchange: "WorkerExchange",
                    routingKey: "server",
                    basicProperties: null,
                    body: body);
            }
            catch (Exception e)
            {
                string error = $"Failed during handling a message. Error message: {e.Message}";

                Console.WriteLine(error);

                channel.BasicAck(args.DeliveryTag, false);

                var body = Encoding.UTF8.GetBytes(error);

                channel.BasicPublish(
                    exchange: "WorkerExchange",
                    routingKey: "server",
                    basicProperties: null,
                    body: body);
            }
        }
    }
}
